import { Pipe, PipeTransform } from '@angular/core';
import {Queueable} from '../interfaces/queueable';

@Pipe({
  name: 'queuedOnly',
  pure: false
})
export class QueuedOnlyPipe implements PipeTransform {

  transform(items: Queueable[] , ...args: any[]): Queueable[] {
    return items.filter(item => {
      return item.queued === args[0];
    });
  }

}
