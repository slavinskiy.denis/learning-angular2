import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pomodoroFormattedTime'
})
export class PomodoroFormattedTimePipe implements PipeTransform {

  transform(totalMinutes: number): any {
    const minutes = totalMinutes % 60;
    const hours = Math.floor(totalMinutes / 60);
    return `${hours} h ${minutes} m`;
  }

}
