import { Injectable } from '@angular/core';

@Injectable()
export class SettingsService {
  timerMinutes: number = 25;
  labelsMap: any;
  pluralsMap: any;

  constructor() {
    this.labelsMap = {
      timer: {
        start: 'Start Timer',
        pause: 'Pause Timer',
        resume: 'Resume Countdown',
        other: 'Unknown'
      }
    };
    this.pluralsMap = {
      tasks: {
        '=0': 'No pomodoros',
        '=1': 'One pomodoro',
        'other': '# pomodoros'
      }
    };
  }

}
