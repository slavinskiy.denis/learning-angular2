import { Injectable } from '@angular/core';
import {Task} from '../interfaces/task';

@Injectable()
export class TaskService {
  public taskStrore: Array<Task> = [];

  constructor() {
    const tasks = [
      {
        name: 'Code an HTML table',
        deadline: 'DEC 03 2017',
        pomodorosRequired: 1
      }, {
        name: 'Sketch a wireframe for the new homepage',
        deadline: 'DEC 08 2017',
        pomodorosRequired: 2
      }, {
        name: 'Style table with Bootstrap styles',
        deadline: 'NOV 24 2017',
        pomodorosRequired: 1
      }, {
        name: 'Reinforce CEO with custom siteMap.xml',
        deadline: 'DEC 05 2017',
        pomodorosRequired: 2
      }
    ];

    this.taskStrore = tasks.map(task => {
      return {
        name: task.name,
        queued: false,
        deadline: new Date(task.deadline),
        pomodorosRequired: task.pomodorosRequired
      };
    });
  }

}
