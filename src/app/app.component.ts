import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  public onPomodoroCompleted() {
    alert('Pomodoro ends');
  }

  public onTick($event) {
    console.log($event);
  }
}
