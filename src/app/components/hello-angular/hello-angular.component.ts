import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello-angular',
  templateUrl: './hello-angular.component.html'
})
export class HelloAngularComponent implements OnInit {
  greeting: string;
  constructor() {
    this.greeting = 'Hello, Angular 2!';
  }

  ngOnInit() {
  }

}
