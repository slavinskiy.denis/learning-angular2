import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SettingsService} from '../shared/services/settings.service';


@Component({
  selector: 'app-pomodoro-timer',
  templateUrl: './timer-widget.component.html',
  styleUrls: ['./timer-widget.component.css'],
})
export class TimerWidgetComponent implements OnInit {
  private _min: number;
  private _sec: number;

  @Output() public complete: EventEmitter<any> = new EventEmitter();
  @Output() public progress: EventEmitter<string> = new EventEmitter();

  isPaused: boolean;
  buttonLabelKey: string;
  buttonLabelMap: any;

  constructor(public settingsService: SettingsService) {}

  private resetPomodoro(): void {
    this._min = this.settingsService.timerMinutes;
    this._sec = 0;
    this.buttonLabelKey = 'start';
  }

  private tick(): void {
    if (!this.isPaused) {
      this.buttonLabelKey = 'pause';
      if (--this._sec < 0) {
        this._sec = 59;
        if (--this._min < 0) {
          this.resetPomodoro();
          this.complete.emit(null);
        }
      }
      this.progress.emit(`${this.min}:${this.sec}`);
    }
  }

  public togglePause(): void {
    this.isPaused = !this.isPaused;
    if (this._min < 24 || this._sec < 59 ) {
      this.buttonLabelKey = this.isPaused ? 'resume' : 'pause';
    }
  }


  get min(): number {
    return this._min;
  }

  get sec(): number {
    return this._sec;
  }

  ngOnInit() {
    this.buttonLabelMap = this.settingsService.labelsMap.timer;
    this.resetPomodoro();
    this.isPaused = true;
    setInterval(() => this.tick(), 1000);
  }
}
