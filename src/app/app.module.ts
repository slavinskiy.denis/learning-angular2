import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HelloAngularComponent } from './components/hello-angular/hello-angular.component';
import { TasksComponent } from './tasks/tasks.component';
import { PomodoroTaskIconsComponent } from './tasks/task-icons.component';
import { TaskTooltipDirective } from './tasks/task-tooltip.directive';
import {TimerWidgetComponent} from './timer/timer-widget.component';
import {QueuedOnlyPipe} from './shared/pipes/queued-only.pipe';
import {SettingsService} from './shared/services/settings.service';
import {TaskService} from './shared/services/task.service';
import {PomodoroFormattedTimePipe} from './shared/pipes/pomodoro-formatted-time.pipe';



@NgModule({
  declarations: [
    AppComponent,
    HelloAngularComponent,
    TimerWidgetComponent,
    TasksComponent,
    PomodoroTaskIconsComponent,
    PomodoroFormattedTimePipe,
    QueuedOnlyPipe,
    TaskTooltipDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [TaskService, SettingsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
