import { Component, OnInit } from '@angular/core';
import {Task} from '../shared/interfaces/task';
import {SettingsService} from '../shared/services/settings.service';
import {TaskService} from '../shared/services/task.service';

@Component({
  selector: 'app-pomodoro-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  today: Date;
  tasks: Task[];
  queuedPomodoros: number;
  queueRenderMapping: any;
  timerMinutes: number;

  constructor(private taskService: TaskService,
              private settingsService: SettingsService) {}

  ngOnInit() {
    this.queueRenderMapping = this.settingsService.pluralsMap.tasks;
    this.tasks = this.taskService.taskStrore;
    this.today = new Date();
    this.timerMinutes = this.settingsService.timerMinutes;
    this.updateQueuedPomodoros();
  }

  toggleTask(task: Task): void {
    task.queued = !task.queued;
    this.updateQueuedPomodoros();
  }

  private updateQueuedPomodoros(): void {
    this.queuedPomodoros = this.tasks
      .filter(task => task.queued)
      .reduce((pomodoros: number, queuedTask: Task) => {
        return pomodoros + queuedTask.pomodorosRequired;
      }, 0);
  }

}
