import {Component, Input, OnInit} from '@angular/core';
import {Task} from '../shared/interfaces/task';

@Component({
  selector: 'app-task-icons',
  template: `<img *ngFor="let icon of icons" src="./assets/img/pomodoro.png" [width]="size" [alt]="icon.name"/>`
})
export class PomodoroTaskIconsComponent implements OnInit {

  @Input() task: Task;
  @Input() size: number;
  icons: Object[] = [];

  constructor() { }

  ngOnInit() {
    this.icons.length = this.task.pomodorosRequired;
    this.icons.fill({name: this.task.name});
  }

}
