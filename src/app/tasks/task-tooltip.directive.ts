import {Directive, HostListener, Input} from '@angular/core';
import {Task} from '../shared/interfaces/task';

@Directive({
  selector: '[task]'
})
export class TaskTooltipDirective {
  private defaultTooltipText: string;
  @Input() task: Task;
  @Input() taskTooltip: any;

  constructor() { }

  @HostListener('mouseover')
  onMouseOver(): void {
    if (this.taskTooltip) {
      this.defaultTooltipText = this.taskTooltip.innerText;
    }
    this.taskTooltip.innerText = this.task.name;
  }

  @HostListener('mouseout')
  onMouseout(): void {
    if (this.taskTooltip) {
      console.log(this.defaultTooltipText);
      this.taskTooltip.innerText = this.defaultTooltipText;
    }
  }
}
